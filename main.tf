
variable "aws_region" {
  default = "eu-west-1"
}
variable "profile" {
  default="test"
}
variable "tags" {
  type = "map"
  default={
    Name             = "lsegbackendtest"

  }
}


resource "aws_s3_bucket" "lseg-backend" {
  bucket="lsegbackendtest"
  acl="private"
  tags="${var.tags}"

  versioning{
    enabled=true
  }

  lifecycle_rule{

    enabled=true
    noncurrent_version_expiration{


      days=30
    }
  }
}
resource "aws_dynamodb_table" "dynamodb-terraform-state-lock_test" {
  name = "statelocktest"
  hash_key = "LockID"
  read_capacity = 20
  write_capacity = 20

  attribute {
    name = "LockID"
    type = "S"
  }
}


