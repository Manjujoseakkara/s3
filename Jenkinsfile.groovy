pipeline
{

    agent any
    stages{

        stage('preflight'){
        
        steps
        {
        echo "preflight"
        sh "chmod 755 exec.sh"
        }
    }

    stage('terraform init')
    {
    steps{
        echo "Terraform init"
        sh "/var/lib/jenkins/workspace/lseg_pipeline/exec.sh init" 
    }
    }

     stage('terraform validate')
    {
    steps{
        echo "Terraform validate"
        sh "/var/lib/jenkins/workspace/lseg_pipeline/exec.sh validate" 
    }

    }
     stage('terraform apply')
    {
    steps{
        echo "Terraform apply"
        sh "/var/lib/jenkins/workspace/lseg_pipeline/exec.sh apply -auto-approve"
    }

    }
}
}